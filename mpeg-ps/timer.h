/* Created by tc2make.  Do not edit, and I mean it! */
#ifndef TIMER_H
#define TIMER_H
#include <stdint.h>
#include <pthread.h>
#include <tcconf.h>

typedef struct tcvp_timer tcvp_timer_t;
#include "driver_timer.h"
typedef tcvp_timer_t *(*timer_new_t)(tcconf_section_t *);
struct tcvp_timer {
    int (*start)(tcvp_timer_t *);
    int (*stop)(tcvp_timer_t *);
    int (*wait)(tcvp_timer_t *, uint64_t, pthread_mutex_t *);
    uint64_t (*read)(tcvp_timer_t *);
    int (*reset)(tcvp_timer_t *, uint64_t);
    int (*interrupt)(tcvp_timer_t *);
    int (*set_driver)(tcvp_timer_t *, timer_driver_t *);
    void (*tick)(tcvp_timer_t *, uint64_t ticks); /* called from driver only */
    int have_driver;
    void *private;
};
#endif
