/**
    Copyright (C) 2003-2006  Michael Ahlberg, Måns Rullgård

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use, copy,
    modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
**/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <tcstring.h>
#include <tctypes.h>
#include <tcalloc.h>
#include <tcendian.h>
#include <pthread.h>
#include <sys/stat.h>
#include "mpeg.h"

struct mpegps_stream {
    MPEG_COMMON;
    FILE *stream;
    int *imap, *map;
    int ps1substr;
    int rate;
    int64_t pts_offset;
    uint64_t time;
    pthread_t eth;
};

// 23:44 <Keiler> url_getu16b() is a special function which is
// essentially fgetc() * 256 + fgetc()
/*
uint16_t *
url_getu16b(FILE *s, uint16_t pkl) {

    return fgetc(s) *256 + fget(pkl);

}
*/
int file_size;
static struct mpegpes_packet *
mpegpes_packet(struct mpegps_stream *s, int pedantic)
{
    FILE *u = s->stream;
    struct mpegpes_packet *pes = NULL;

    do {
        uint32_t stream_id;
        u_int scode = 0, zc = 0, i = pedantic? 3: 0x10000;
        uint16_t pkl;
        int pklen;

        do {
            scode = getc(u);
            if(scode == 0){
                zc++;
            } else if(zc >= 2 && scode == 1){
                break;
            } else if(scode < 0){
                printf("MPEGPS: zc=%i, scode=%x\n",
                          zc, scode);
                return NULL;
            } else {
                zc = 0;
            }
        } while(!scode || i--);

        if(zc < 2 || scode != 1){
            printf("MPEGPS: zc=%i, scode=%x\n",
                      zc, scode);
            return NULL;
        }

        stream_id = getc(u);

        if(stream_id == PACK_HEADER){
            int b = getc(u) & 0xc0;
            char foo[8];
            if(b == 0x40){
                int sl;
                fread(foo, 1, 8, u);
                sl = getc(u) & 7;
                while(sl--)
                    getc(u);
            } else {
                fread(foo, 1, 7, u);
            }
            continue;
        } else if(stream_id == 0xb9){
            uint64_t p = ftell(u);
            printf("MPEGPS: end code @ %llu (%llx)\n",
                      p, p);
            continue;
            return NULL;
        } else if(stream_id < 0xba){
            uint64_t p = ftell(u);
            printf("MPEGPS: unknown PES id %x @ %lli (%llx)\n", stream_id, p, p);
            continue;
        }
       // url_getu16b(u, &pkl);
        pklen = getc(u)*256 + getc(u);

        if(stream_id == PROGRAM_STREAM_MAP){
            pes = malloc(sizeof(*pes));
            pes->stream_id = 0xbc;
            pes->data = malloc(pklen);
            pes->hdr = pes->data;
            pes->flags = 0;
            pklen = fread(pes->data, 1, pklen, u);
            if(pklen < 0)
                return NULL;
            pes->size = pklen;
        } else if(ISVIDEO(stream_id) || ISMPEGAUDIO(stream_id) ||
                  stream_id == PRIVATE_STREAM_1){
            pes = malloc(sizeof(*pes));
            pes->hdr = malloc(pklen);
            pklen = fread(pes->hdr, 1, pklen, u);
            if(pklen < 0)
                return NULL;
            pes->stream_id = stream_id;
            mpegpes_header(pes, pes->hdr, 6);
            pes->size = pklen - (pes->data - pes->hdr);
            if(pes->stream_id == PRIVATE_STREAM_1 && s->ps1substr){
                pes->stream_id = *pes->data;
            }
        } else {
            char foo[pklen];
            if(fread(foo, 1, pklen, u) < pklen){
                return NULL;
            }
        }
    } while(!pes);

    return pes;
}

static void
mpegpes_free(struct mpegpes_packet *p)
{
    free(p->hdr);
    free(p);
}

static void
mpegps_free_pk(void *v)
{
    tcvp_data_packet_t *p = v;
    struct mpegpes_packet *mp = p->private;
    mpegpes_free(mp);
}

extern tcvp_packet_t *
mpegps_packet(muxed_stream_t *ms, int str)
{
    struct mpegps_stream *s = ms->private;
    struct mpegpes_packet *mp = NULL;
    tcvp_data_packet_t *pk;
    int sx = -1;

    do {
        if(mp)
            mpegpes_free(mp);
        if(!(mp = mpegpes_packet(s, 0)))
            return NULL;

        sx = s->imap[mp->stream_id];

        if(ISAC3(mp->stream_id) || ISDTS(mp->stream_id)){
            mp->data += 4;
            mp->size -= 4;
        } else if(ISPCM(mp->stream_id)){
            int aup = htob_16(unaligned16(mp->data + 2));
            mp->data += 7;
            mp->size -= 7;
            if(sx >= 0 && mp->flags & PES_FLAG_PTS)
                mp->pts -= 27000000LL * aup / ms->streams[sx].common.bit_rate;
        } else if(ISSPU(mp->stream_id)){
            mp->data++;
            mp->size--;
        }
    } while(sx < 0 || !ms->used_streams[sx]);

    pk = malloc(sizeof(*pk));
    pk->stream = sx;
    pk->data = &mp->data;
    pk->sizes = &mp->size;
    pk->planes = 1;
    pk->flags = 0;
    pk->private = mp;

    if(mp->flags & PES_FLAG_PTS){
        mp->pts += s->pts_offset;
        mp->dts += s->pts_offset;
        pk->pts = mp->pts * 300;
        pk->flags |= TCVP_PKT_FLAG_PTS;
        if(mp->pts)
            s->rate = ftell(s->stream) * 90 / mp->pts;
        s->time = mp->pts;
    }

    if(mp->flags & PES_FLAG_DTS){
        pk->dts = mp->dts * 300;
        pk->flags |= TCVP_PKT_FLAG_DTS;
    }

    return (tcvp_packet_t *) pk;
}

static uint64_t
get_time(struct mpegps_stream *s)
{
    struct mpegpes_packet *mp;
    uint64_t ts = -1;
    int bc = 0;

    do {
        if(!(mp = mpegpes_packet(s, 0)))
            break;
        if(mp->flags & PES_FLAG_PTS)
            ts = mp->pts;
        mpegpes_free(mp);
    } while(ts == -1 && bc++ < 256);

    return ts;
}

#define absdiff(a, b) ((a)>(b)? (a)-(b): (b)-(a))

static uint64_t
mpegps_seek(muxed_stream_t *ms, uint64_t time)
{
    struct mpegps_stream *s = ms->private;
    int64_t p, st, lp, lt, op;
    int64_t tt, d;
    FILE *u = s->stream;

    time /= 300;
    tt = time - 90000;
    if(tt < 0)
        tt = 0;

    p = time * s->rate / 90;
    if(p > file_size || p <= 0)
        p = file_size / 2;
    d = p < file_size / 2? p / 2: (file_size - p) / 2;

    st = lt = get_time(s);
    op = lp = ftell(u);

    printf("MPEGPS: seek %lli->%lli, %lli->%lli\n",
              lt / 90000, tt / 90000, op, p);

    while(absdiff(lt, tt) > 90000 && d > 1000){
        if(fseek(u, p, SEEK_SET)){
            printf("MPEGPS: seek failed %lli\n", p);
            goto err;
        }

        st = get_time(s);
        if(st == -1)
            goto err;

        p = ftell(u);

        printf("MPEGPS: seek %lli @%lli d=%lli\n",
                  st / 90000, p, d);

        if(p == lp)
            break;

        lp = p;
        lt = st;

        if(st < tt)
            p += d;
        else
            p -= d;
        d /= 2;

        if(p > file_size)
            break;
    }

    while(st < time){
        p = ftell(u);
        st = get_time(s);
        if(st == -1)
            goto err;
    }

  out:
    fseek(u, p, SEEK_SET);
    s->pts_offset = 0;

    printf("MPEGPS: seek @ %llu (%llx)\n", p, p);

    return st * 300;
  err:
    fseek(u, op, SEEK_SET);
    return -1;
}

static void *
mpegps_event(void *p)
{
    struct mpegps_stream *s = p;
    int run = 1;

    while(run){
		printf("Running ");
		/*
        if(te->type == TCVP_BUTTON){
            tcvp_button_event_t *be = (tcvp_button_event_t *) te;
            if(be->button == 1 &&
               be->action == TCVP_BUTTON_PRESS)
        } else if(te->type == TCVP_KEY){
            tcvp_key_event_t *ke = (tcvp_key_event_t *) te;
            if(!strcmp(ke->key, "escape"))
        } else if(te->type == -1){
            run = 0;
        }
		*/
    }

    return NULL;
}

static void
mpegps_free(void *p)
{
    muxed_stream_t *ms = p;
    struct mpegps_stream *s = ms->private;

    if(s->stream)
        free(s->stream);
    free(s->imap);
    free(s->map);

    free(s);

    mpeg_free(ms);
}

static int
mpegps_findpsm(muxed_stream_t *ms, int ns)
{
    struct mpegps_stream *s = ms->private;
    stream_t *sp = ms->streams;
    struct mpegpes_packet *pk = NULL;
    u_char *pm;
    int l, pc = 0;

    printf("MPEGPS: Searching for PSM MPEGPS \n");

    do {
        if(pk)
            mpegpes_free(pk);
        if(!(pk = mpegpes_packet(s, 1))){
            break;
        }
    } while(pk->stream_id != PROGRAM_STREAM_MAP && pc++ < 16);

    if(!pk || pk->stream_id != PROGRAM_STREAM_MAP){
        printf("MPEGPS: PSM not found after %i packets, giving up \n", pc - 1);
        if(pk)
            mpegpes_free(pk);
        return -1;
    }

    printf("MPEGPS: PSM found at packet %i \n", pc - 1);

    pm = pk->data + 2;
    l = htob_16(unaligned16(pm));
    pm += l + 2;
    l = htob_16(unaligned16(pm));
    pm += 2;

    while(l > 0){
        u_int stype = *pm++;
        u_int sid = *pm++;
        u_int il = htob_16(unaligned16(pm));
        const struct mpeg_stream_type *mst;

        pm += 2;

        if(ms->n_streams == ns){
            ns *= 2;
            ms->streams = realloc(ms->streams, ns * sizeof(*ms->streams));
            sp = &ms->streams[ms->n_streams];
        }

        s->imap[sid] = ms->n_streams;
        s->map[ms->n_streams] = sid;

        if((mst = mpeg_stream_type_id(stype, mpeg_stream_types)) != NULL){
            memset(sp, 0, sizeof(*sp));
            if(!strncmp(mst->codec, "video/", 6))
                sp->stream_type = STREAM_TYPE_VIDEO;
            else
                sp->stream_type = STREAM_TYPE_AUDIO;
            sp->common.codec = mst->codec;
            sp->common.index = ms->n_streams++;
            sp->common.start_time = -1;

            if(sid == PRIVATE_STREAM_1)
                s->ps1substr = 0;

            printf("MPEGPS: stream %x type %02x\n", sid, stype);

            mpeg_parse_descriptors(ms, sp, NULL, pm, il);
            pm += il;

            sp++;
        } else {
            pm += il;
            l -= il;
        }
        l -= 4;
    }
    mpegpes_free(pk);
    return 0;
}

static int
mpegps_findstreams(muxed_stream_t *ms, int ns)
{
    struct mpegps_stream *s = ms->private;
    stream_t *sp = ms->streams;
    struct mpegpes_packet *pk = NULL;
    int pc = 0;
    printf("MPEGPS: Searching for streams\n");
/*
 * So here we have made as 128 instead of unkown const
 * tcvp_demux_mpeg_conf_ps_search_packets because as per tcvp 0.2.0's mpegps.c
 * contains the condition as 
 *         while(pc++ < 128){
 *            if(!(pk = mpegpes_packet(u, 1))){.......}}
 */
    while(pc++ < 128){
        if(!(pk = mpegpes_packet(s, 1))){
            break;
        }

        if(ISVIDEO(pk->stream_id) || ISMPEGAUDIO(pk->stream_id) ||
           ISAC3(pk->stream_id) || ISDTS(pk->stream_id) ||
           ISPCM(pk->stream_id) || ISSPU(pk->stream_id) ||
           ISPS1AC3(pk)){
            if(s->imap[pk->stream_id] < 0){
                if(ms->n_streams == ns){
                    ns *= 2;
                    ms->streams =
                        realloc(ms->streams, ns * sizeof(*ms->streams));
                    sp = &ms->streams[ms->n_streams];
                }

                memset(sp, 0, sizeof(*sp));
                s->imap[pk->stream_id] = ms->n_streams;
                s->map[ms->n_streams] = pk->stream_id;

                printf("\n Find MPEGPS Stream \n found stream id %02x @ packet %i\n",
                          pk->stream_id, pc - 1);

                if(ISVIDEO(pk->stream_id)){
                    u_int scode = -1;
                    u_char *p;

                    sp->stream_type = STREAM_TYPE_VIDEO;
                    sp->common.codec = "video/mpeg";

                    for(p = pk->data; p < pk->data + pk->size - 5; p++){
                        scode <<= 8;
                        scode |= *p;
                        if((scode & ~0xff) == 0x100){
                            if(scode == 0x1b0 && (p[3] != 0 || p[4] != 1)){
                                sp->common.codec = "video/cavs";
                                break;
                            }
                        }
                    }
                } else if(ISMPEGAUDIO(pk->stream_id)){
                    sp->stream_type = STREAM_TYPE_AUDIO;
                    sp->common.codec = "audio/mpeg";
                } else if(ISAC3(pk->stream_id)){
                    sp->stream_type = STREAM_TYPE_AUDIO;
                    sp->common.codec = "audio/ac3";
                } else if(ISDTS(pk->stream_id)){
                    sp->stream_type = STREAM_TYPE_AUDIO;
                    sp->common.codec = "audio/dts";
                } else if(ISPCM(pk->stream_id)){
                    sp->stream_type = STREAM_TYPE_AUDIO;
                    sp->common.codec = "audio/pcm-s16be";
                    if((pk->data[5] & 0x30) == 0){
                        sp->audio.sample_rate = 48000;
                    } else if((pk->data[5] & 0x30) == 1){
                        sp->audio.sample_rate = 96000;
                    } else {
                        printf("MPEGPS: unknown PCM sample rate %i \n",
                                  pk->data[5] & 0x30);
                        sp->audio.sample_rate = 48000;
                    }
                    sp->audio.channels = (pk->data[5] & 0x7) + 1;
                    sp->audio.bit_rate =
                        sp->audio.channels * sp->audio.sample_rate * 16;
                } else if(ISSPU(pk->stream_id)) {
                    sp->stream_type = STREAM_TYPE_SUBTITLE;
                    sp->common.codec = "subtitle/dvd";
                } else if(ISPS1AC3(pk)){
                    sp->stream_type = STREAM_TYPE_AUDIO;
                    sp->common.codec = "audio/ac3";
                }
                sp->common.index = ms->n_streams++;
                sp->common.start_time = -1;
                sp++;
            }
        } else {
            printf("MPEGPS: unhandled stream id %02x\n", pk->stream_id);
        }
        mpegpes_free(pk);
    }

    return 0;
}

extern muxed_stream_t *
mpegps_open(char *name, FILE *u, tcconf_section_t *cs, tcvp_timer_t *tm)
{
    muxed_stream_t *ms;
    struct mpegps_stream *s;
    int ns, i;
    int nonspu = 0;
    printf("Inside mpegps_open \n");
    ms = malloc(sizeof(*ms));
    ms->next_packet = mpegps_packet;
    ms->seek = mpegps_seek;
    s = calloc(1, sizeof(*s));
    ms->private = s;
    ns = 2;
    ms->streams = calloc(ns, sizeof(*ms->streams));
    s->imap = malloc(0x100 * sizeof(*s->imap));
    memset(s->imap, 0xff, 0x100 * sizeof(*s->imap));
    s->map = malloc(0x100 * sizeof(*s->map));
    memset(s->map, 0xff, 0x100 * sizeof(*s->map));

    s->stream = u;
    s->ps1substr = 1;
    printf("Mapping and memalloc done \n");
    if(mpegps_findpsm(ms, ns)){
        printf("MPEGPS: Found a PSM case \n");
        fseek(u, 0, SEEK_SET);
        mpegps_findstreams(ms, ns);
    }
      printf("MPEGPS: Outside PSM case \n");
    if(!ms->n_streams){
        fseek(u, 0, SEEK_SET);
        free(ms);
        return NULL;
    }

    for(i = 0; i < ms->n_streams; i++){
        printf("MPEGPS: map %x -> %i\n",
                  s->map[i], i);
        if(ms->streams[i].stream_type != STREAM_TYPE_SUBTITLE)
            nonspu++;
    }

    for(i = 0; i < ms->n_streams; i++){
        ms->streams[i].common.flags |= TCVP_STREAM_FLAG_TRUNCATED;
        if(ms->streams[i].stream_type == STREAM_TYPE_SUBTITLE && nonspu)
            ms->streams[i].common.flags |= TCVP_STREAM_FLAG_NOBUFFER;
    }

    printf("MPEGPS: at %ld\n",
              ftell(s->stream));

/* if(!(u->flags & URL_FLAG_STREAMED) && 10485760 > 1048576){
 * Removing the URL_FLAG_STREAMED check condition as it will be only local
 * building and testing.
 */
    printf("FILE SIZE INSIDEEE: %d", file_size);
    if(file_size > 1048576){
        uint64_t stime, etime = -1, tt;
        uint64_t spos, epos;

        printf("MPEGPS: determining stream length\n");

        fseek(u, 0, SEEK_SET);
        stime = get_time(s);
        spos = ftell(u);

        printf("MPEGPS: start timestamp %lli us @%lli\n",
                  stime / 27, spos);

        fseek(u, -1048576, SEEK_END);
        while((tt = get_time(s)) != -1)
            etime = tt;
        epos = ftell(u);

        printf("MPEGPS: end timestamp %lli us @%lli\n",
                  etime / 27, epos);

        if(stime != -1 && etime != -1){
            uint64_t dt = etime - stime;
            uint64_t dp = epos - spos;
            s->rate = dp * 90 / dt;
            ms->time = 300LL * dt;
        }
    }

        fseek(s->stream, 0, SEEK_SET);

    return ms;
}


int main(int argc, char *argv[]){
    printf("\nMain Loop Init; \n");
    char* path_file;
    struct stat st;
    FILE *file = fopen(argv[1], "r");
    if (file == NULL){
        printf("Cannot open file \n");
        exit(0);
    }
    if (file != NULL){
        path_file = realpath(argv[1], NULL);
        printf("File Opened: %s \n", path_file);
        free(path_file);
    }

    stat(path_file, &st);
    file_size = st.st_size;
    printf("File Size: %d \n", file_size);

    tcvp_timer_t *timer_object;
    tcconf_section_t *section_oject;

    mpegps_open("test",file, section_oject, timer_object);
    return 0;
}
