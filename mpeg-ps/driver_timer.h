/* Created by tc2make.  Do not edit, and I mean it! */
#ifndef DRIVER_TIMER_H
#define DRIVER_TIMER_H
typedef struct timer_driver timer_driver_t;
#include "timer.h"
typedef timer_driver_t *(*driver_timer_new_t)(tcconf_section_t *, int resolution);
struct timer_driver {
    int (*start)(timer_driver_t *);
    int (*stop)(timer_driver_t *);
    int (*set_timer)(timer_driver_t *, tcvp_timer_t *);
    void *private;
};
#endif
