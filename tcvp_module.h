/* Created by tc2make.  Do not edit, and I mean it! */
#ifndef TCVP_MODULE_H
#define TCVP_MODULE_H
#include <tcconf.h>

typedef struct tcvp_module tcvp_module_t;
struct tcvp_module {
    int (*init)(tcvp_module_t *);
    void *private;
};
typedef tcvp_module_t *(*tcvp_module_new_t)(tcconf_section_t *);
#endif
