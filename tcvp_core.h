/* Created by tc2make.  Do not edit, and I mean it! */
#ifndef TCVP_CORE_H
#define TCVP_CORE_H
#include "tcvp_module.h"
#include <tctypes.h>
#include <tcalloc.h>
#include "tcvp_types.h"

typedef struct tcvp_event {
    int type;
} tcvp_event_t;

typedef struct tcvp_key_event {
    int type;
    char *key;
} tcvp_key_event_t;

typedef struct tcvp_open_event {
    int type;
    char *file;
} tcvp_open_event_t;

typedef struct tcvp_open_multi_event {
    int type;
    int nfiles;
    char **files;
} tcvp_open_multi_event_t;

typedef struct tcvp_seek_event {
    int type;
    int64_t time;
    int how;
} tcvp_seek_event_t;

#define TCVP_SEEK_ABS  0
#define TCVP_SEEK_REL  1

typedef struct tcvp_timer_event {
    int type;
    uint64_t time;
} tcvp_timer_event_t;

typedef struct tcvp_state_event {
    int type;
    int state;
} tcvp_state_event_t;

#define TCVP_STATE_PLAYING 0
#define TCVP_STATE_END     1
#define TCVP_STATE_ERROR   2
#define TCVP_STATE_STOPPED 3
#define TCVP_STATE_PL_END  4

typedef struct tcvp_load_event {
    int type;
    muxed_stream_t *stream;
} tcvp_load_event_t;

typedef struct tcvp_button_event {
    int type;
    int button;
    int action;
    int x, y;
} tcvp_button_event_t;

#define TCVP_BUTTON_PRESS   0
#define TCVP_BUTTON_RELEASE 1
typedef tcvp_module_t *(*tcvp_core_new_t)(tcconf_section_t *);
#endif
